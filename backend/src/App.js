const express = require("express");

const app = express();
const bodyParser = require('body-parser');

const services = require('./services');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.post('/add', (req, res) => {
    const empID = Math.random().toString(36).substring(7);
    services.adds(empID, req.body);
    res.send("data is added!! with empID : " + empID);
});


app.get('/', (req, res) => {
    const empolyes = services.getList();
    res.send(empolyes);
});

app.delete('/delete/:empId', (req, res) => {
    services.deleteEmp(req.params.empId)
    res.send("deleted");
});

app.get('/details/:empId', (req, res) => {

    var details;
    try {
        details = services.getDetails(req.params.empId)
    }
    catch (e) {
        details = "No data available for this employee";
    }
    res.send(details);
});

app.put('/update/:empId', (req, res) => {

    var details;
    try {
        details = services.updateDetails(req.params.empId, req.body)
    }
    catch (e) {
        details = "No data available for this employee";
    }
    res.send(details);
});


if (app.listen(8080))
    console.log("app is running on port 8080")