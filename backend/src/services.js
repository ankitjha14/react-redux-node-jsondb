var JsonDB = require('node-json-db');
var db = new JsonDB("myDataBase", true, false);

function adds(empId, data) {
    db.push('/' + empId, data);
}

function getList() {
    var data = db.getData("/");
    return data;
}

function deleteEmp(empId) {
    db.delete("/" + empId);
}

function getDetails(empId) {
    var data = db.getData("/" + empId);
    return data;
}


function updateDetails(empId,data) {
    db.push("/" + empId, data, false);
}


module.exports = {adds, getList, deleteEmp, getDetails, updateDetails};